json.array!(@readings) do |reading|
  json.extract! reading, :id, :flow_measure, :temperature_measure, :fan_set, :temperature_set, :created_at,:setpoint,:gain,:integ,:deriv,:setpointH,:gainH,:integH,:derivH
  json.url reading_url(reading, format: :json)
end
