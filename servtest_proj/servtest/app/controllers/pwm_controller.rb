class PwmController < ApplicationController
  skip_before_filter :verify_authenticity_token


  def create
    @pwm=Pwm.first
    @pwm.update_attributes(pwm_temp:params[:pwm_temp], pwm_flow:params[:pwm_flow],setpoint:params[:setpoint],gain:params[:gain],integ:params[:integ],deriv:params[:deriv],openloop:params[:openloop],setpointH:params[:setpointH],gainH:params[:gainH],integH:params[:integH],derivH:params[:derivH])
    @pwm.save




  end


  def index

    @pwm=Pwm.all
    respond_to do |format|

      format.html # show.html.erb
      format.json { render :json => @pwm }
    end
  end



  def show
    @pwm=Pwm.first
   respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @pwm }
    end
  end


  def update

  end

end
