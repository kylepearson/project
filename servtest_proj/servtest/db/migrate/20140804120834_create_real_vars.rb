class CreateRealVars < ActiveRecord::Migration
  def change
    create_table :real_vars do |t|
      t.float :temp
      t.float :flow

      t.timestamps
    end
  end
end
