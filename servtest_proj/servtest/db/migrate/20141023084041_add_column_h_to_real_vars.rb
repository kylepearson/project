class AddColumnHToRealVars < ActiveRecord::Migration
  def change
    add_column :real_vars, :gainH, :float
    add_column :real_vars, :tiH, :float
    add_column :real_vars, :tdH, :float
  end
end
