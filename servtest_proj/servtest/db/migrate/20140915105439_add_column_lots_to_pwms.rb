class AddColumnLotsToPwms < ActiveRecord::Migration
  def change
    add_column :pwms, :setpoint, :float
    add_column :pwms, :gain, :float
    add_column :pwms, :integ, :float
    add_column :pwms, :deriv, :float
    add_column :pwms, :openloop, :int
  end
end
