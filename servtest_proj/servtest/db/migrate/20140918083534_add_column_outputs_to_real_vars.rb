class AddColumnOutputsToRealVars < ActiveRecord::Migration
  def change
    add_column :real_vars, :setpoint, :float
    add_column :real_vars, :gain, :float
    add_column :real_vars, :ti, :float
    add_column :real_vars, :td, :float
  end
end
