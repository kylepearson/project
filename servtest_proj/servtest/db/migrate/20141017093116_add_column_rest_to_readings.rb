class AddColumnRestToReadings < ActiveRecord::Migration
  def change
    add_column :readings, :setpoint, :float
    add_column :readings, :gain, :float
    add_column :readings, :integ, :float
    add_column :readings, :deriv, :float
    add_column :readings, :setpointH, :float
    add_column :readings, :gainH, :float
    add_column :readings, :integH, :float
    add_column :readings, :derivH, :float
    add_column :readings, :openloop, :int
  end
end
