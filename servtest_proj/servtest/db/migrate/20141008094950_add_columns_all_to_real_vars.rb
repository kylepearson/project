class AddColumnsAllToRealVars < ActiveRecord::Migration
  def change
    add_column :real_vars, :tempset, :float
    add_column :real_vars, :flowset, :float
    add_column :real_vars, :setpointH, :float

  end
end
