class CreatePwms < ActiveRecord::Migration
  def change
    create_table :pwms do |t|
      t.float :pwm_temp
      t.float :pwm_flow

      t.timestamps
    end
  end
end
