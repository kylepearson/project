class CreateReadings < ActiveRecord::Migration
  def change
    create_table :readings do |t|
      t.float :flow_measure
      t.float :temperature_measure
      t.float :fan_set
      t.float :temperature_set

      t.timestamps
    end
  end
end
