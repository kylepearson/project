class ChangeIntegFieldInPwm < ActiveRecord::Migration
  def self.up
    change_column :pwms, :integ, :decimal, :precision=>5, :scale=>2
    change_column :pwms, :integH, :decimal, :precision=>5, :scale=>2
    change_column :pwms, :deriv, :decimal, :precision=>5, :scale=>2
    change_column :pwms, :derivH, :decimal, :precision=>5, :scale=>2
  end
  def self.down
    change_column :pwms, :integ, :float
    change_column :pwms, :integH, :float
    change_column :pwms, :deriv, :float
    change_column :pwms, :derivH, :float
  end
end
