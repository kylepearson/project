class AddColumnHeatstuffToPwms < ActiveRecord::Migration
  def change
    add_column :pwms, :setpointH, :float
    add_column :pwms, :gainH, :float
    add_column :pwms, :integH, :float
    add_column :pwms, :derivH, :float
  end
end
