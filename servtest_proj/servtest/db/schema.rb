# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141023084041) do

  create_table "pwms", force: true do |t|
    t.float    "pwm_temp",   limit: 24
    t.float    "pwm_flow",   limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "setpoint",   limit: 24
    t.float    "gain",       limit: 24
    t.decimal  "integ",                 precision: 5, scale: 2
    t.decimal  "deriv",                 precision: 5, scale: 2
    t.integer  "openloop"
    t.float    "setpointH",  limit: 24
    t.float    "gainH",      limit: 24
    t.decimal  "integH",                precision: 5, scale: 2
    t.decimal  "derivH",                precision: 5, scale: 2
  end

  create_table "readings", force: true do |t|
    t.float    "flow_measure",        limit: 24
    t.float    "temperature_measure", limit: 24
    t.float    "fan_set",             limit: 24
    t.float    "temperature_set",     limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "setpoint",            limit: 24
    t.float    "gain",                limit: 24
    t.float    "integ",               limit: 24
    t.float    "deriv",               limit: 24
    t.float    "setpointH",           limit: 24
    t.float    "gainH",               limit: 24
    t.float    "integH",              limit: 24
    t.float    "derivH",              limit: 24
    t.integer  "openloop"
  end

  create_table "real_vars", force: true do |t|
    t.float    "temp",       limit: 24
    t.float    "flow",       limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "setpoint",   limit: 24
    t.float    "gain",       limit: 24
    t.float    "ti",         limit: 24
    t.float    "td",         limit: 24
    t.float    "tempset",    limit: 24
    t.float    "flowset",    limit: 24
    t.float    "setpointH",  limit: 24
    t.float    "gainH",      limit: 24
    t.float    "tiH",        limit: 24
    t.float    "tdH",        limit: 24
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

end
